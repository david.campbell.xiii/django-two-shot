from django.contrib import admin
from django.urls import path
from receipts.views import (
    show_receipt,
    create_receipt,
    account_list,
    category_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("", show_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
